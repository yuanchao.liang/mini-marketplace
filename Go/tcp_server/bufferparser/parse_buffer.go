package bufferparser

import (
	"fmt"
	"net"
	"strconv"
)

var bufferHeadLen = 3
var invalidOper byte = '3'
var maxBufferLen = 1024

func ParseBuffer(conn net.Conn) (uint8, string, string) {
	buf := make([]byte, maxBufferLen)
	readBufLen, err := conn.Read(buf)
	if err != nil {
		fmt.Println("Failed to read data from buffer!")
	}
	if readBufLen <= bufferHeadLen {
		return invalidOper, "", ""
	}
	readBuffer := string(buf[:readBufLen])
	operType := readBuffer[0]
	para1Len, _ := strconv.Atoi(readBuffer[1:bufferHeadLen])
	para1, para2 := readBuffer[bufferHeadLen:bufferHeadLen+para1Len], readBuffer[bufferHeadLen+para1Len:]
	return operType, para1, para2
}
