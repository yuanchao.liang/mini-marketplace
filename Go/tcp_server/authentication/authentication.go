package authentication

import (
	"database/sql"
	"fmt"
	"math/rand"
	"minimarktcp/tcp_server/dbtools"
	"strconv"
	"time"
)

var globalAuthenticCnt int64 = 0
var globalTotalLoginTime int64 = 0
var globalTotalTokenCheckTime int64 = 0
var globalSyncTime = time.Now()
var globalTimePrintFrequency int64 = 1000
var maxInt64Num = int64(9223372036854775807)

func generateRandomToken() string {
	rand.Seed(time.Now().UnixNano())
	return strconv.FormatInt(rand.Int63n(maxInt64Num), 10)
}

func CheckToken(userName string, token string) string {
	tStart := time.Now()
	globalAuthenticCnt++
	defer func() {
		globalTotalTokenCheckTime += time.Since(tStart).Milliseconds()
		authenticTimeDataPrinter()
	}()
	/* Get token from redis */
	name, err := dbtools.GetFromRedis(token)
	if err != nil || name != userName {
		return "Failed"
	} else {
		return "Succeed"
	}
}

func CheckUserIsRegistered(userName string) *sql.Rows {
	db, err := dbtools.ConnectToMySQL()
	if err != nil {
		fmt.Println("Failed to connect MySQL.", err)
	}
	/* Start query */
	sqlQuery := "SELECT user_name FROM prodintf_user_login_tab WHERE user_name='" + userName + "';"
	rows, qErr := db.Query(sqlQuery)
	if qErr != nil {
		fmt.Println("Failed: Select from table failed", qErr)
	}
	return rows
}

func RegisterNewUser(userName, password string) string {
	ret := ""
	db, err := dbtools.ConnectToMySQL()
	if err != nil {
		return "Failed to connect MySQL"
	}
	sqlInsert := "insert into prodintf_user_login_tab values('" + userName + "', '" + password + "')"
	_, err = db.Exec(sqlInsert)
	if err != nil {
		ret = "Failed: Register new user to database failed!"
	} else {
		tcpToken := generateRandomToken()
		ret = "Succeed" + tcpToken
		dbtools.InsertToRedis(tcpToken, userName, time.Hour)
	}
	return ret
}

func Register(userName, password string) string {
	ret := ""
	rows := CheckUserIsRegistered(userName)
	defer func(rows *sql.Rows) {
		closeErr := rows.Close()
		if closeErr != nil {
			fmt.Println("Close query failed!", closeErr)
		}
	}(rows)
	if rows.Next() { /* User registered, prevent repeat registering */
		ret = "Failed: User " + userName + " have been registered before!"
	} else { /* Register new user */
		ret = RegisterNewUser(userName, password)
	}
	return ret
}

func loginViaRedis(userName, password string) string {
	ret := "Failed"
	result, err := dbtools.GetFromRedis(userName)
	if err != nil {
		ret = "Failed to get from redis"
	} else if result != password {
		ret = "Failed, User not exists or password incorrect!"
	} else {
		tcpToken := generateRandomToken()
		ret = "Succeed" + tcpToken
		dbtools.InsertToRedis(tcpToken, userName, time.Hour)
	}
	return ret
}

func loginViaMySQLCheck(userName, password string) *sql.Rows {
	db, err := dbtools.ConnectToMySQL()
	if err != nil {
		fmt.Println("Failed to connect to MySQL.", err)
	}
	/* Start query */
	sqlQuery := "SELECT user_name FROM prodintf_user_login_tab WHERE user_name='" + userName + "' AND password='" + password + "';"
	rows, err := db.Query(sqlQuery)
	if err != nil {
		fmt.Println("Failed to select from table.", err)
	}
	return rows
}

func loginViaMySQL(userName, password string) string {
	ret := ""
	rows := loginViaMySQLCheck(userName, password)
	defer func(rows *sql.Rows) {
		closeErr := rows.Close()
		if closeErr != nil {
			fmt.Println("Close query failed!", closeErr)
		}
	}(rows)
	/* Login or abort */
	if !rows.Next() { /* User not exists or password incorrect */
		ret = "Failed, user not exists or password incorrect!"
	} else { /* User login succeed */
		tcpToken := generateRandomToken()
		ret = "Succeed" + tcpToken
		dbtools.InsertToRedis(tcpToken, userName, time.Hour)
		dbtools.InsertToRedis(userName, password, time.Hour)
	}
	return ret
}

func Login(userName, password string) string {
	tStart := time.Now()
	defer func() {
		globalTotalLoginTime += time.Since(tStart).Milliseconds()
		authenticTimeDataPrinter()
	}()

	/* Login from Redis cache directly, improving the performance */
	loginFromRedisResult := loginViaRedis(userName, password)
	if loginFromRedisResult[:len("Succeed")] == "Succeed" {
		return loginFromRedisResult
	}

	/* Login via MySQL */
	loginViaMySQLResult := loginViaMySQL(userName, password)
	if loginViaMySQLResult[:len("Succeed")] == "Succeed" {
		return loginViaMySQLResult
	}
	return "Failed, invalid username or password!"
}

func authenticTimeDataPrinter() {
	if time.Since(globalSyncTime).Milliseconds() < globalTimePrintFrequency || globalAuthenticCnt == 0 {
		return
	}
	fmt.Println(
		"Avg login time: ", globalTotalLoginTime/globalAuthenticCnt, "ms, ",
		"Avg token check time: ", globalTotalTokenCheckTime/globalAuthenticCnt, "ms.")
	globalSyncTime = time.Now()
}
