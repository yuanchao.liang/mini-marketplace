package dbtools

import (
	"database/sql"
	"fmt"
	"github.com/go-redis/redis"
	_ "github.com/go-sql-driver/mysql"
	"time"
)

var globalRedisDb *redis.Client
var globalMySQLDb *sql.DB
var globalMySQLDbErr error

func init() {
	globalRedisDb = redis.NewClient(
		&redis.Options{
			Addr:     "127.0.0.1:6379",
			Password: "",
			DB:       0,
			/* Size settings */
			PoolSize:     32,
			MinIdleConns: 10,
			/* Time out */
			DialTimeout:  time.Second * 5,
			ReadTimeout:  time.Second * 3,
			WriteTimeout: time.Second * 3,
			PoolTimeout:  time.Second * 4,
			/* Check */
			IdleCheckFrequency: time.Second * 60,
			IdleTimeout:        time.Minute * 5,
			MaxConnAge:         time.Second * 0,
			/* Retry */
			MaxRetries:      0,
			MinRetryBackoff: 8 * time.Millisecond,
			MaxRetryBackoff: 512 * time.Millisecond,
		})
	globalMySQLDb, globalMySQLDbErr = sql.Open("mysql", "root@tcp(127.0.0.1:3306)/mini_market_db")
	globalMySQLDb.SetMaxOpenConns(100)
	globalMySQLDb.SetMaxIdleConns(100)
	globalMySQLDb.SetConnMaxLifetime(time.Minute * 10)
	globalMySQLDb.SetConnMaxIdleTime(time.Minute * 10)
	fmt.Println("Initialized DB")
}

func InsertToRedis(key, value string, expireTime time.Duration) {
	for globalRedisDb.SetNX(key, value, expireTime).Val() {
		// fmt.Println("Set failed, locked and retrying.")
	}
}

func GetFromRedis(key string) (string, error) {
	ret, err := globalRedisDb.Get(key).Result()
	return ret, err
}

func ConnectToMySQL() (*sql.DB, error) {
	return globalMySQLDb, globalMySQLDbErr
}
