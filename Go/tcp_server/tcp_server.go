package main

import (
	"fmt"
	"minimarktcp/tcp_server/authentication"
	"minimarktcp/tcp_server/bufferparser"
	"net"
	"time"
)

/* Connection management */
func handleConn(conn net.Conn) {
	defer func(conn net.Conn) {
		err := conn.Close()
		if err != nil {

		}
	}(conn)
	operType, para1, para2 := bufferparser.ParseBuffer(conn)

	ret := ""
	switch operType {
	case '0':
		ret = authentication.Login(para1, para2)
	case '1':
		ret = authentication.Register(para1, para2)
	case '2':
		ret = authentication.CheckToken(para1, para2)
	}
	_, err := conn.Write([]byte(ret))
	if err != nil {
		fmt.Println("Send data to client failed.")
	}
	time.Sleep(time.Millisecond * 500)
}

func main() {
	/* Listen TCP */
	listener, err := net.Listen("tcp", "127.0.0.1:8888")
	if err != nil {
		fmt.Println("TCP listener create failed!")
	}
	/* Keep handling TCP connection */
	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println("TCP accept failed!")
		}
		go handleConn(conn)
	}
}
