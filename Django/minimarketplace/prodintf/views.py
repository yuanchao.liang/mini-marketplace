# -*- coding: utf-8 -*-
import datetime

from models import user_info, prod_info, prod_details, prod_comment
from django.shortcuts import render
from django.http import HttpResponse
from django.utils.safestring import mark_safe
from api import authentication, data_process


def login_and_register_page(request):
    return render(request, "index.html")


def authenticate(request):
    name = request.POST.get('username')
    encoded_password = authentication.encode_sha256(request.POST.get('password'))

    # Check name and password filled
    if name == "" or encoded_password == authentication.encode_sha256(""):
        return render(request, "index.html", {"message": "Please input username or password!"})

    # Login or register
    if request.POST.has_key("register"):
        auth_ret = authentication.auth_by_tcp("register", name, encoded_password)
    else:  # Beside pressing login button, pressing enter can also call this
        auth_ret = authentication.auth_by_tcp("login", name, encoded_password)

    # TCP verification
    succeed_keyword = "Succeed"
    if auth_ret[:len(succeed_keyword)] != succeed_keyword:
        return render(request, "index.html", {"message": auth_ret})

    # Verified succeed
    auth_str = auth_ret[len(succeed_keyword):].strip()
    authentication.set_access_token_cookies(request, auth_str, name)
    response = show_products(request, 0, "", "")
    response.set_cookie(key="tcptoken", value=auth_str)
    response.set_cookie(key="username", value=name)
    if request.POST.has_key("register"):
        user_info(name=name).save()
    return response


def show_products(request, page, keyword, category):
    # Token to authenticate user
    if not authentication.user_tcp_token_check(request):
        return render(request, "index.html", {"message": ""})

    # Show products
    user_name_welcome_str = ""
    try:
        user_name_welcome_str += "Hi, "
        if request.POST.has_key('register'):
            user_name_welcome_str += "new user "
        user_name_welcome_str += request.POST.get('username').encode('utf-8')
    except:
        pass

    is_test_user_name = False
    try:
        user_name_str = request.POST.get('username').encode('utf-8')
        if int(user_name_str) <= 200:
            is_test_user_name = True
    except:
        pass
    if is_test_user_name:  # For the performance test, render frontier html is not needed
        ret = HttpResponse(content="0")
    else:
        ret = render(request, "products.html", {
            'user_name': user_name_welcome_str,
            'table': mark_safe(
                data_process.query_to_html(
                    data_process.get_queryset(keyword, category), page, "product_table"
                )
            )
        })
    return ret


def search(request):
    try:
        page = int(request.POST.get('page'))
    except:
        page = 1
    category = request.POST.get('category')
    keyword = request.POST.get('keyword')
    return show_products(request, page - 1, keyword, category)


def get_reply_user_name(request):
    try:
        init_comment = "@" + request.GET.get("username") + " "
    except:
        init_comment = ""
    return init_comment


def get_pid_from_request(request):
    try:
        pid = int(request.GET.get("pid"))
    except:
        pid = request.COOKIES["pid"]
    return pid


def details(request):
    # Token to authenticate user
    if not authentication.user_tcp_token_check(request):
        return render(request, "index.html", {"message": ""})

    # Getting information of product from different tables
    pid = get_pid_from_request(request)
    prod_info_table = prod_info.objects.filter(pid=pid).values()[0]
    prod_details_table = prod_details.objects.filter(pid=pid).values()[0]
    prod_comment_table = prod_comment.objects.filter(pid=pid).values()

    # Organize the shown information from table
    prod_dict = dict()
    prod_dict["prod_title"] = prod_info_table["title"].encode('utf-8')
    prod_dict["prod_price"] = str(prod_info_table["price"])
    prod_dict["prod_category"] = prod_info_table["category"]
    prod_dict["prod_image"] = prod_info_table["image_file_name"]
    prod_dict["prod_description"] = prod_details_table["description"].encode('utf-8')
    prod_dict["init_comment"] = get_reply_user_name(request)
    prod_dict["prod_comment_table"] = mark_safe(
        data_process.query_to_html(
            data_process.format_comment_table(prod_comment_table), 0, "comment_table"
        )
    )

    # Showing detail and comment(include reply button) of products, creating a sending comment interface
    response = render(request, "details.html", prod_dict)
    response.set_cookie(key="pid", value=pid)
    return response


def comment(request):
    # Insert comment
    user_name = request.COOKIES["username"]
    uid = user_info.objects.filter(name=user_name).values()[0]["uid"]
    pid = request.COOKIES["pid"]
    comment_content = request.POST.get("comment")
    prod_comment(uid=uid, comment=comment_content, pid=pid).save()

    # Refresh product page
    response = details(request)
    response.set_cookie(key="pid", value=pid)
    return response
