from django.db import models


class user_info(models.Model):
    # User table models
    uid = models.IntegerField(default=0, primary_key=True)
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name


class prod_category(models.Model):
    # Product related table models
    category = models.CharField(max_length=32, primary_key=True)


class prod_info(models.Model):
    pid = models.IntegerField(default=0, primary_key=True)
    title = models.CharField(max_length=256)
    price = models.IntegerField(default=0)
    image_file_name = models.CharField(max_length=256)
    category = models.CharField(max_length=32)


class prod_details(models.Model):
    pid = models.IntegerField(default=0, primary_key=True)
    description = models.CharField(max_length=1024)


class prod_comment(models.Model):
    cid = models.IntegerField(default=0, primary_key=True)
    comment = models.CharField(max_length=2048)
    uid = models.IntegerField(default=0)
    pid = models.IntegerField(default=0)