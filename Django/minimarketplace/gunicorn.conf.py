import multiprocessing

bind = "127.0.0.1:8000"
workers = 7

threads = multiprocessing.cpu_count() * 2
worker_class = "gevent"
worker_connections = 1000
backlog = 2048
keepalive = 10
max_requests = 2048
timeout = 1200
graceful_timeout = 300
preload_app = True
limit_request_line = 0

errorlog = '/Users/yuanchao.liang/gunicorn/gunicorn_log_error/gunicorn.error.log'
accesslog = '/Users/yuanchao.liang/gunicorn/gunicorn_log_access/gunicorn.access.log'
proc_name = 'gunicorn_project'