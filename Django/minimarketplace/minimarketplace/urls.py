from django.conf.urls import patterns, url
from prodintf import views

urlpatterns = patterns('',
    url(r"^admin/", views.login_and_register_page),
    url(r"^index/", views.authenticate),
    url(r"^search/", views.search),
    url(r"^details", views.details),
    url(r"^comment", views.comment),
)
