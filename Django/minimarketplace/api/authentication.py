import hashlib
import socket

HOST_IP = "127.0.0.1"
TCP_PORT = 8888
MIN_TCP_LEN = len("Failed")
MAX_BUFFER_LEN = 1024


def form_to_tcp_buffer(oper_type, para1, para2):
    # Use 0, 1, 2 to represent login, register, check token respectively
    oper_char = {"login": "0", "register": "1", "check token": "2"}
    buffer_chars = [oper_char[oper_type]]

    # Convert length of para1 to buffer
    para1_len = str(len(para1))
    buffer_chars.append("0" + para1_len if len(para1_len) == 1 else para1_len)
    buffer_chars.append(para1 + para2)
    return bytes("".join(buffer_chars))


def auth_by_tcp(oper_type, name, password):
    # Transfer data to buffer
    buffer_chars = form_to_tcp_buffer(oper_type, name, password)

    # Connect to server
    receive = ""
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        client.connect((HOST_IP, TCP_PORT))
        client.send(buffer_chars)
        receive = str(client.recv(MAX_BUFFER_LEN))
    except:
        pass
    finally:
        client.close()

    # Capture receive data from TCP server
    if receive != "" and len(receive) < MIN_TCP_LEN:
        return "Invalid receive length from TCP!"
    return receive


def set_access_token_cookies(request, token, user_name):
    request.COOKIES["tcptoken"] = token
    request.COOKIES["username"] = user_name


def encode_sha256(data):
    sha256_encoder = hashlib.sha256()
    sha256_encoder.update(data)
    return sha256_encoder.hexdigest()


def user_tcp_token_check(request):
    user_name = request.COOKIES["username"].strip()
    tcp_token = request.COOKIES["tcptoken"].strip()
    auth_ret = auth_by_tcp("check token", user_name, tcp_token)
    success_keyword = "Succeed"
    success_len = len(success_keyword)
    return len(auth_ret) >= success_len and auth_ret[:success_len] == success_keyword
