import datetime

from prodintf.models import user_info, prod_info

PAGE_ITEM_MAX = 50


def format_table_head(queryset):
    s = '''<tr">\n'''
    for elem in queryset[0]:
        s += '''<td>'''
        s += elem
        s += '''</td>\n'''
    s += "<td>details</td>"
    s += '''<tr>\n'''
    return s


def format_table_type(column, elem, table_type):
    s = ""
    if column == "title" and table_type == "product_table":
        s += "<td><a href=\"../details?pid=" + str(elem["pid"])
        s += "\" target=\"_black\"><h3>Show details</h3></a></td>"
    if column == "user_name" and table_type == "comment_table":
        s += "<td><a href=\"../details?username=" + elem["user_name"].encode('utf-8')
        s += "\"><h3>Reply</h3></a></td>"
    return s


def format_table_data(queryset, page, table_type):
    s = ""
    for i in range(min(PAGE_ITEM_MAX, len(queryset) - PAGE_ITEM_MAX * page)):
        elem = queryset[i + page * PAGE_ITEM_MAX]
        s += '''<tr>\n'''
        for column in elem:
            s += '''<td>'''
            e = elem[column]
            s += e.encode('utf-8') if isinstance(e, unicode) else str(e)
            s += '''</td>\n'''
            s += format_table_type(column, elem, table_type)
        s += '''</tr>\n'''
    return s


def query_to_html(queryset, page, table_type):
    if len(queryset) == 0:
        return ""
    s = '''<table border='1'>'''
    s += format_table_head(queryset) + format_table_data(queryset, page, table_type)
    s += "</table>"
    return s


def format_comment_table(comment_table):
    format_comments = []
    for comment_item in comment_table:
        try:
            new_comment_item = dict()
            new_comment_item["user_name"] = user_info.objects.filter(uid=int(comment_item["uid"])).values()[0]["name"]
            new_comment_item["comment"] = comment_item["comment"]
            format_comments.append(new_comment_item)
        except:
            pass
    return format_comments


def get_queryset(keyword, category):
    if keyword not in ["", None]:
        if category in ["", None]:
            return prod_info.objects.filter(title__icontains=keyword).values()
        return prod_info.objects.filter(title__icontains=keyword, category=category).values()
    elif category not in ["", None]:
        return prod_info.objects.filter(category=category).values()
    ret = prod_info.objects.all().values()
    return ret
