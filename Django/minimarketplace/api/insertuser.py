import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "minimarketplace.settings")
from prodintf.models import user_info
import authentication
import pymysql.cursors


def insert_user_info_table():
    for i in range(201, 10 ** 7 + 1):
        user_info(name=str(i)).save()
        print "Inserting", i


def insert_user_and_password():
    connection = pymysql.connect(
        host='127.0.0.1', port=3306,
        user='root', password='',
        db='mini_market_db',
        charset='utf8',
        cursorclass=pymysql.cursors.DictCursor
    )
    for i in range(72976, 10 ** 7 + 1):
        user_name, password = str(i), authentication.encode_sha256(str(i))
        sql = "insert into prodintf_user_login_tab values('%s','%s')" % (user_name, password)
        insert_success = False
        while not insert_success:
            try:
                cursor = connection.cursor()
                cursor.execute(sql)
                connection.commit()
                insert_success = True
                print "Insert user %s succeed!" % user_name
            except:
                pass
            finally:
                cursor.close()
    connection.close()


# Adding 10 million users
if __name__ == "__main__":
    insert_user_info_table()
    insert_user_and_password()
