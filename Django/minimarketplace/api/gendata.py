import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'minimarketplace.settings')
from prodintf.models import prod_category, prod_info, prod_details

MIN_CRAWL_NAME_LEN = 60
ITEM_ID_MAX = 2 ** 31 - 1


def getdatafromtxt(path, category):
    data = []
    keywords = ["itemid:", "name:", "price:"]
    last_keyword = ""
    with open(path) as f:
        for line in f.readlines():
            for word in keywords:
                if line[:len(word)] != word:
                    continue
                if word == "name:" and len(line) < MIN_CRAWL_NAME_LEN:
                    break
                valid_data = line.strip()
                if word == "itemid:" and last_keyword == "itemid:":
                    data[-1] = valid_data
                else:
                    data.append(valid_data)
                    last_keyword = word
                if word == "price:":
                    data.append(category)
    return data


def formatdata(data):
    offset, format_data = 0, []
    for i in range(int(len(data) / 4)):
        try:
            format_data.append({
                "itemid": int(data[4 * i + offset].replace("\"", "")[7:]),
                "name": data[4 * i + offset + 1][5:],
                "price": int(data[4 * i + offset + 2][6:]) / 100000,
                "category": data[4 * i + offset + 3]
            })
        except:
            if 3 * i + offset >= len(data):
                break
            while data[3 * i + offset][:7] != "itemid:":
                offset += 1
    return format_data


def crawlshopeedata(num_of_pages, categories):
    cur_dir = os.getcwd()
    data = []
    for i in range(num_of_pages):
        data += getdatafromtxt(os.path.join(cur_dir, "search_from_shopee_{}.txt".format(i)), categories[i])
    return formatdata(data)


if __name__ == "__main__":
    categories = [
        "Apple", "Huawei", "Xiaomi", "Samsung", "Shopee", "Battery", "USB", "Phone case", "Game device",
        "Hahaha", "Bags", "Sport", "Video", "Camera phone", "Men", "Women", "Others"
    ]
    craw_data = crawlshopeedata(num_of_pages=len(categories), categories=categories)

    # Insert items to tables
    for elem in categories:
        prod_category(category=elem).save()

    for i in range(len(craw_data)):
        elem = craw_data[i]
        try:
            prod_info(
                pid=elem["itemid"] % ITEM_ID_MAX,
                title=elem["name"].replace("\"", ""),
                price=elem["price"],
                image_file_name="{}.jpg".format(i),
                category=elem["category"]
            ).save()

            prod_details(
                pid=elem["itemid"] % ITEM_ID_MAX,
                description="My description:\n{}\nprice[{}], category:[{}]\n".format(elem["name"], elem["price"], elem["category"])
            ).save()
        except:
            print "Not support inserting data with facial expressions:", elem["name"]
